# Demo App

This project is a multi-module starter project that presents both a Spring Boot RESTful application and an Angular
client.

The Angular client was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Running in (eg) IntelliJ

Start the Spring Boot application (ServerApplication) - IntelliJ, despite using gradle to run the application, will not
benefit from the configuration put in place to embed the Angular client in the server-side application so only the
RESTful application is exposed at this point.

The Angular application can then be started separately by navigating inside the _client_ directory and running
`ng serve` ... which means that _live-reload_ works as expected. A proxy config is also provided so that back end
requests route to the Spring Boot application.

The client application will be available at http://localhost:4200 and for REST Clients the server's example endpoint is
available at http://localhost:8080/api/welcome

## Running via Gradle

From the project root run `./gradlew bootRun`.

The client and server applications are now both available at the same base location http://localhost:8080.

## Running via Java

Build the project from the project root by running `./gradlew clean build` then run it via
`java -jar ./server/api/build/libs/api-0.0.1-SNAPSHOT.jar` using at least Java 11.

## NOTES

* The project is deliberately trivial/nonsense to minimise the refactoring required to get the first real-world
interaction up and running.
* Basic routing has been implemented but currently only caters for a single "home" page. For applications that do not
require routing it **might** (?) be desirable to remove this?
* The server application does not currently make use of any Java 11 specific features so could easily be downgraded to
older versions.
