import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

interface WelcomeResponse {

  time: string;
  data: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  serverTime: string;
  data: string = 'hi';

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit(): void {
    this.httpClient.get<WelcomeResponse>('/api/welcome', {responseType: 'json'}).subscribe(welcome => {
      this.serverTime = welcome.time;
      this.data = welcome.data;
    });
  }
}
