package uk.co.nesstechnology.demo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import uk.co.nesstechnology.demo.services.config.ServiceConfig;

@SpringBootApplication
@Import(ServiceConfig.class)
public class ServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(ServerApplication.class, args);
  }
}
