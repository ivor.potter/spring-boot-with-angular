package uk.co.nesstechnology.demo.api.model;

import java.time.Instant;

// TODO Lombok
public class Welcome {

  private final Instant time;

  private final String data;

  public Welcome(final String data) {
    this.time = Instant.now();
    this.data = data;
  }

  public Instant getTime() {
    return time;
  }

  public String getData() {
    return data;
  }
}
