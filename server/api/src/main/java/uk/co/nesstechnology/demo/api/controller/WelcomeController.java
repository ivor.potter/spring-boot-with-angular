package uk.co.nesstechnology.demo.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.co.nesstechnology.demo.api.model.Welcome;
import uk.co.nesstechnology.demo.services.service.IdService;

@RestController
@RequestMapping("/api")
public class WelcomeController {

  private final IdService idService;

  public WelcomeController(final IdService idService) {
    this.idService = idService;
  }

  @GetMapping("/welcome")
  ResponseEntity<Welcome> welcome() {

    return ResponseEntity.ok(new Welcome(idService.generateId()));
  }
}
