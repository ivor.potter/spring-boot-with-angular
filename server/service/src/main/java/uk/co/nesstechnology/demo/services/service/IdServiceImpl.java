package uk.co.nesstechnology.demo.services.service;

import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public class IdServiceImpl implements IdService {

  @Override
  public String generateId() {

    return UUID.randomUUID().toString();
  }
}
