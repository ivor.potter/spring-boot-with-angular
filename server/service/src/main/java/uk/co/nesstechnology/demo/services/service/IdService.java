package uk.co.nesstechnology.demo.services.service;

public interface IdService {

  String generateId();
}
