package uk.co.nesstechnology.demo.services.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("uk.co.nesstechnology.demo.services.service")
public class ServiceConfig {}
